﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models.VesselVisit
{
    public class CreateVesselVisitViewModel
    {
        public string VesselVisitNumber { get; set; }
        public string Linea { get; set; }

    }
}