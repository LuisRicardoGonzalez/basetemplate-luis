﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCBase.Models.VesselVisit;

namespace MVCBase.Controllers
{
    public class VesselVisitController : Controller
    {
        // GET: VesselVisit
        [HttpGet]
        public ActionResult Index()
        {
            return View(new CreateVesselVisitViewModel());
        }

        [HttpPost]
        public ActionResult Index(CreateVesselVisitViewModel modeloCreacionVesselVisit)
        {
            if (ModelState.IsValid)
            {
                
            }
            return RedirectToAction("Index");
        }
    }
}